Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  resource :session, only: [:create, :destroy, :options]
  resource :user, only: [:create, :show, :destroy]
  resource :product, only: [:create, :show, :destroy, :rate] do
  	resource :rate, only: [:create]
  end
end
