class CreateProducts < ActiveRecord::Migration[5.2]
  def change
    create_table :products do |t|
      t.string :name
      t.references :user, index: true, foreign_key: {on_delete: :cascade}
      t.decimal :price, null:false, default: 0
      t.integer :total_rating, null:false, default: 0
      t.integer :total_rating_count, null:false, default: 0
      t.string :image_url, :string, default: 'http://via.placeholder.com/400x300'

      t.timestamps
    end
  end
end
