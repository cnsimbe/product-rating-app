class CreateRatings < ActiveRecord::Migration[5.2]
  def change
    create_table :ratings do |t|
      t.references :user, index: true, foreign_key: {on_delete: :cascade}
      t.references :product, index: true, foreign_key: {on_delete: :cascade}
      t.integer :value, null:false, default: 0

      t.timestamps
    end
  end
end
