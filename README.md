# ProductRating web app
### Clone the repository
##### `git clone https://gitlab.com/cnsimbe/product-rating-app.git`
##
### Then install and run the dev application:
##### `bundle install`
##### `rails db:migrate`
##### `rails server`
##
### Using the app
Open your browser and navigate to <u>http://localhost:3000</u> where the web app will be served

After logging in/signing up, on the product list page, the bottom-right floating button navigates to the 'Create Product' page, while the top-right button is the 'logout' button
##