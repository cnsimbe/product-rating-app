class ApplicationController < ActionController::Base
	helper_method :current_user, :auth_required

  	private
	    def current_user
	      session[:user_id]
	    end

	    def login_user(user_id)
	    	session[:user_id] = user_id
	    end

	    def require_login
	      if !current_user
	      	render json: { message: :"User not logged in", status: :unauthorized }
	      end
	    end

end
