class SessionsController < ApplicationController
	skip_before_action :verify_authenticity_token
	
	def create
		@user = User.find_by(users_params)
		if @user
			login_user @user.id
			render json: {id: @user.id }
		else
			render json: {message: :"Invalid credentials"}, status: :unauthorized
		end
	end

	def destroy
		session.delete(:user_id)
		render json: {success: true}
	end


	private
	  def users_params
	    params.permit(:username, :password)
	  end

end

