class UsersController < ApplicationController
	skip_before_action :verify_authenticity_token

	def show
		@user_id = current_user
		if @user_id
			render json: {id: @user_id }
		else
			render json: {message: :"User not logged in"}, status: :unauthorized
		end
	end

	def create
		@user = User.new(users_params)
		begin
			if @user.save
				login_user @user.id
				render json: {id: @user[:id] }
			else
				render json: {message: :"Unable to create account"}, status: :internal_server_error
			end
		rescue
			render json: {message: :"Unable to create account. User already exists"}, status: :internal_server_error
		end
	end

	private
	  def users_params
	    params.permit(:username, :password)
	  end

end
