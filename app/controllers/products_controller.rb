class ProductsController < ApplicationController
	skip_before_action :verify_authenticity_token
	before_action :require_login

	def show
		render json: Product.all
	end

	def create
		@product = Product.new product_params
		@product.user_id = current_user
		if @product.save
			@product.image_url = rails_blob_path(@product.image, only_path: true)
			@product.save
			render json: @product
		else
			render json: {message: :"Error creating product", status: :internal_server_error}
		end
	end

	def destroy
		product = Product.find_by(id:params[:id], user_id: current_user)
		if product
			product.delete
			render json: {success: true}
		else
			render json: {message: :"Product not found", status: :internal_server_error }
		end

	end
	
	private
	  def product_params
	    params.permit(:name, :price, :image)
	  end

end
