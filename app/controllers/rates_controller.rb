class RatesController < ApplicationController
	skip_before_action :verify_authenticity_token
	before_action :require_login

	
	def create
		@rparams = rate_params
		@product = Product.find_by(id:@rparams[:id])
		if @product
			@product.with_lock do
				rating = Rating.find_by(user_id: current_user, product_id: @product.id)
				if !rating
					rating = Rating.new(user_id: current_user, product_id: @product.id, value: 0)
					@product.total_rating_count +=  1
				end

				@product.total_rating += (@rparams[:value] - rating.value)
				@product.save!

				rating.value = @rparams[:value]
				rating.save!
			end
			render json: @product
		else
			render json: {message: :"Product not found", status: :internal_server_error }
		end
	end



	private
	  def rate_params
	    params.permit(:id, :value)
	  end

end
