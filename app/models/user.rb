class User < ApplicationRecord


	has_many :products, dependent: :delete_all
	has_many :ratings, dependent: :delete_all

	validates :username, presence: true
	validates :password, presence: true, length: { minimum: 5 }

end
