class Rating < ApplicationRecord
  belongs_to :user
  belongs_to :product

  validates :value, presence: true, numericality: { only_integer: true }
  validates :user_id, presence: true, numericality: { only_integer: true }
  validates :product_id, presence: true, numericality: { only_integer: true }

end
