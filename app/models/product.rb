class Product < ApplicationRecord
	belongs_to :user

	has_many :ratings, dependent: :delete_all

	has_one_attached :image

	validates :name, presence: true, length: { minimum: 2 }
	validates :price, presence: true, numericality: { greater_than: 0 }
	validates :image, presence: true

end