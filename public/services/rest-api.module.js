angular.module('restApi',[])

angular
	.module('restApi')
	.service('restApi',function($http,$rootScope){
		
		$rootScope.user = null;
		
		this.login = (username,password)=>{
			return $http.post('/session', {username, password}, { headers: {'Content-Type': 'application/json'} })
			.then(res=>res.data)
			.then(user=>$rootScope.user=user)
		}



		this.logout = ()=>{
			return $http.delete('/session')
			.then(user=>$rootScope.user=null)
		}

		
		this.getProducts = ()=> {
			return $http.get('/product')
			.then(res=>res.data)
		}


		this.rateProduct = (id,value)=> {
			return $http.post('/product/rate', {value,id}, { headers: {'Content-Type': 'application/json'} })
			.then(res=>res.data)
		}


		this.createProduct = (product)=> {
			let fd = new FormData()
			for(let i in product)
				fd.append(i,product[i])


			return $http.post('/product', fd, {headers: {'Content-Type': undefined}})
			.then(res=>res.data)
		}

		
		this.deleteProduct = (id)=> {
			return $http.delete('/product', { params: {id} })
			.then(res=>res.data)
		}

		this.createUser = (username,password)=> {
			return $http.post('/user', {username, password}, {headers: {'Content-Type': 'application/json'} })
			.then(res=>res.data)
			.then(user=>$rootScope.user=user)
		}

		this.getUser = ()=> {
			return $http.get('/user')
			.then(res=>res.data)
			.then(user=>$rootScope.user=user)
		}

	})