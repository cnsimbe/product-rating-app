angular.
  module('productRating').
  config(function($locationProvider, $provide, $httpProvider, $routeProvider) {

      let authRequired = function($q,$location,$rootScope,restApi){
        if($rootScope.user)
          return $q.resolve($rootScope.user)
        return restApi.getUser()
          .catch(error=>{
            $location.path('/login')
            throw error
          })
      }

      $routeProvider.
        when('/login', {
          template: '<login></login>',

        }).
        when('/signup', {
          template: '<sign-up></sign-up>'
        }).
        when('/product-list', {
          template: '<product-list></product-list>',
          resolve: { user : authRequired }
        }).
        when('/product-create', {
          template: '<product-create></product-create>',
          resolve: { user : authRequired }
        }).
        otherwise('/product-list');


        $provide.factory('restInterceptor', function($q,$rootScope) {
        return {
         responseError: function(rejection) {
            console.log(rejection)
            if(rejection.status == 401)
              $rootScope.user = null;
            if(!rejection.message && rejection.data && rejection.data.message)
              rejection.message = rejection.data.message
            return $q.reject(rejection);
          }
        };
      });

      $httpProvider.interceptors.push('restInterceptor');
    }
  );