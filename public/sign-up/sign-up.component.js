angular
	.module('signUp')
	.component('signUp',{
		templateUrl: 'sign-up/sign-up.template.html',
		controller: function($scope,restApi,$location,$window) {
			$scope.username = null;
			$scope.password = null;

			$scope.onSubmit = function($event){

				$event.preventDefault();
				$event.stopPropagation();
				
				if(!$scope.signInForm.$valid)
					return $window.alert("Fill in the fields correctly")
				
				restApi.createUser($scope.username,$scope.password)
				.then(user=>$location.path('/product-list'))
				.catch(error=>$window.alert(error.message));
			}

			$scope.goToLogin = ()=>$location.path("/login")
		}
	})