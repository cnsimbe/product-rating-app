angular
	.module('productCreate')
	.component('productCreate',{
		templateUrl: 'product-create/product-create.template.html',
		controller: function($scope,restApi,$location,$window,$rootScope) {
			$scope.product = {};

			$scope.onSubmit = ($event)=>{

				$event.preventDefault();
				$event.stopPropagation();

				if(!$scope.productForm.$valid)
					return $window.alert("Fill in the fields correctly")

				restApi.createProduct($scope.product)
				.then(()=>$location.path("/product-list"))
				.catch(error=>$window.alert(error.message));
			}

			let fileElm = $window.document.getElementById("create-product-image")
			fileElm.onchange = ($event)=>{
				$scope.product.image = fileElm.files[0]
			}
		}
	})