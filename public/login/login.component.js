angular
	.module('login')
	.component('login',{
		templateUrl: 'login/login.template.html',
		controller: function($scope,restApi,$location,$window) {
			$scope.username = null;
			$scope.password = null;

			$scope.onSubmit = function($event){

				
				$event.preventDefault();
				$event.stopPropagation();
				
				if(!$scope.loginForm.$valid)
					return $window.alert("Fill in the fields correctly")

				restApi.login($scope.username,$scope.password)
				.then(user=>$location.path('/product-list'))
				.catch(error=>$window.alert(error.message));
				
			}

			$scope.goToSignUp = ()=>$location.path("/signup")
		}
	})