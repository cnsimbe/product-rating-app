angular
	.module('productList')
	.component('productList',{
		templateUrl: 'product-list/product-list.template.html',
		controller: function($scope,restApi,$location,$window,$rootScope) {
			$scope.items = [];

			$scope.getProducts = (index)=>{
				return restApi.getProducts()
				.then(items=>$scope.items=items)
			}

			$scope.deleteProduct = (item)=>{
				if($rootScope.user.id != item.user_id)
					return $window.alert("Item can only be deleted by the owner")
				let confirm = $window.confirm("Are you sure you want to delete this product?")
				if(confirm)
					restApi.deleteProduct(item.id)
						.then(()=>$scope.getProducts($scope.index))
						.catch(error=>$window.alert(error.message))
			}

			$scope.rateProduct = (item)=>{
				let rating = parseInt($window.prompt("Enter in your rating for the product:"))
				if(isNaN(rating))
					return $window.alert("Invalid input")

				restApi.rateProduct(item.id,rating)
				.then(product=>{
					item.total_rating = product.total_rating
					item.total_rating_count = product.total_rating_count
				})


			}

			$scope.logout = ()=> restApi.logout().then(()=>$location.path("/login"))

			$scope.goToCreate = ()=>$location.path("/product-create")

			$scope.getProducts($scope.index)
		}
	})